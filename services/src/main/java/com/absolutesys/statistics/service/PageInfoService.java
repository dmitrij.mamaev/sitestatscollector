package com.absolutesys.statistics.service;

import com.absolutesys.statistics.db.powerbi.Utils;
import com.absolutesys.statistics.domain.StatsItem;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

public class PageInfoService {

    @Autowired
    Utils utils;

    // Persist info on site seconds
    public void collectStats(java.security.Principal user, JSONObject jsonInput) {
        String userName = user.getName();
        String previousPage = jsonInput.getAsString("previousPage");
        String currentPage = jsonInput.getAsString("currentPage");
        // Event type,
        StatsItem statsItem = new StatsItem(previousPage, currentPage);
        statsItem.setUser(userName);
        utils.gather(statsItem);

    }
}