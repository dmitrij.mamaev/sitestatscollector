package com.absolutesys.statistics;

import com.absolutesys.statistics.service.PageInfoService;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

public class Controllers {

    @Autowired
    PageInfoService service;

    @RestController
    public class StatsController {

        // The controller will return by necessity from JSON Object
        @PostMapping("/logPageInfo")
        @Produces("application/json")
        @Consumes("application/json")
        public JSONObject logPageStats(java.security.Principal user, JSONObject inputJson) throws Exception {
            service.collectStats(user, inputJson);
            return null;
        }
    }

}
