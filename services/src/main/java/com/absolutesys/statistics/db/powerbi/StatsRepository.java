package com.absolutesys.statistics.db.powerbi;

import com.absolutesys.statistics.domain.SiteStatistics;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StatsRepository extends MongoRepository<SiteStatistics, String> {

}
