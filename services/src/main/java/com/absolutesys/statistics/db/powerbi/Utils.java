package com.absolutesys.statistics.db.powerbi;

import com.absolutesys.statistics.domain.SiteStatistics;
import com.absolutesys.statistics.domain.StatsItem;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {

    @Autowired
    private StatsRepository statsRepository;

    private StatsItem currentStatsItem;
    List<StatsItem> list = Collections.synchronizedList(new ArrayList<StatsItem>());
    List<SiteStatistics> statsList = Collections.synchronizedList(new ArrayList<SiteStatistics>());

    public void gather(StatsItem statsItem) {
        this.currentStatsItem = statsItem;
        calculateTime();
        list.add(statsItem);
        statsRepository.saveAll(statsList);
    }

    private synchronized void calculateTime() {
        SiteStatistics stats = new SiteStatistics();
        List<StatsItem> forThisPerson = list.stream().
                filter(p -> p.getUser().equals(currentStatsItem.getUser())).
                collect(Collectors.toList());

        StatsItem lastItem = forThisPerson.get(forThisPerson.size() - 1);
        // For previous - must be previous page for current user
        if (!lastItem.getCurrentPage().equals(currentStatsItem.getPreviousPage())) {
            // We went to another page
            long millisSpent = currentStatsItem.getMillis() - lastItem.getMillis();
            stats.setMillis(millisSpent);
        }
        stats.setUser(currentStatsItem.getUser());
        stats.setPageURI(currentStatsItem.getCurrentPage());
        // Add statistics entry
        statsList.add(stats);
    }
}
