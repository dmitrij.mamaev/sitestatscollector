package com.absolutesys.statistics.domain;

public class SiteStatistics {
    private String user;
    private String pageURI;
    private long millis;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPageURI() {
        return pageURI;
    }

    public void setPageURI(String pageURI) {
        this.pageURI = pageURI;
    }

    public long getMillis() {
        return millis;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }
}
