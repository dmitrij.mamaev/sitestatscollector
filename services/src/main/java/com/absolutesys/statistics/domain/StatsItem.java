package com.absolutesys.statistics.domain;

public class StatsItem {
    private String previousPage;
    private String currentPage;
    private String user;
    private long millis;
    public StatsItem(String previousPage, String currentPage) {
        this.previousPage = previousPage;
        this.currentPage = currentPage;
        this.millis = System.currentTimeMillis();
    }

    public long getMillis() {
        return millis;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }

    public StatsItem(String user, String previousPage, String currentPage) {
        this.user = user;
        this.previousPage = previousPage;
        this.currentPage = currentPage;
    }


    public String getPreviousPage() {
        return previousPage;
    }

    public void setPreviousPage(String previousPage) {
        this.previousPage = previousPage;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
